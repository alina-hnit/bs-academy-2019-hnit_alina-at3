const ListPage = require('../page/List_po');
const page = new ListPage();

class ListActions {

    clickUpdateButton() {
        page.updateButton.waitForDisplayed(2000);
        page.updateButton.click();
    }

}

module.exports = ListActions;
