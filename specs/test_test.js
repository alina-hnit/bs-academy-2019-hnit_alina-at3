const credentials = require('../testData.json');
const HelpClass = require('../helpers/helpers');
const AssertHelper = require('../helpers/validators');
const CustomWaits = require('../helpers/waiters');
const MenuActions = require('./Menu/actions/menu_pa');
const LoginPage = require('./Login/page/Login_po');
const NewPlacePage = require('./NewPlace/page/NewPlace_po');
const ListActions = require('./Lists/actions/List_pa');

const loginPage = new LoginPage();
const newPlacePage = new NewPlacePage();
const listPage = new ListActions();
const help = new HelpClass();
const validate = new AssertHelper();
const wait = new CustomWaits();
const menu = new MenuActions();

describe('test', () => {

    
    afterEach(() => {
        browser.reloadSession();
    });

    xit('should represent all items in list', () => {
        
        help.loginWithDefaultUser(credentials);
        wait.forSpinner();
        // validate.elementCountIs();
        
    });
    
    xit('should display validation fail on field', () => {
        help.loginWithCustomUser(credentials.password, credentials.email);
        validate.wrongValueIndicationOnField(loginPage.emailInput);
        
    });

    xit('should display validation fail on label', () => {
        help.loginWithDefaultUser(credentials);
        wait.forSpinner();
        menu.navigateToNewPlace();
        newPlacePage.nextButton.click();

        validate.wrongValueIndicationOnLable(newPlacePage.nameFieldLable);
    });
    
    it('should edit a list', () => {
        
        help.loginWithDefaultUser(credentials);
        wait.forSpinner();
        menu.navigateToLists();
     
        wait.forListsToBeShown();
        help.startEditingItemInList('Editable');

        listPage.clickUpdateButton();
        validate.successNotificationTextIs('The list was updated!');
        
    });
    
    

//    it('should not login with wrong password', () => {
//
//        
//
//    });

    

});