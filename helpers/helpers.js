const LoginActions = require('../specs/Login/actions/Login_pa');
const page = new LoginActions();
const credentials = require('./../testData.json');

class HelpClass 
{

    clickItemInList(name) {
        const place = $$(`//div[contains(@class, "place-item")]//h3/a[contains(., "${name}")]`);
        if (place.length === 0) {
            throw new Error("Element not found");
        }
        place[0].scrollIntoView();
        place[0].click();
    }

    browserClick(elm){
        return browser.execute((e) => {
            document.querySelectorAll(e).click();
        }, elm);
    }

    browserClickOnArrayElement(elm, index){
        return browser.execute((e, i) => {
            document.querySelectorAll(e)[i - 1].click();
        }, elm, index);
    }

    loginWithDefaultUser() {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');

        page.enterEmail(credentials.email);
        page.enterPassword(credentials.password);
        page.clickLoginButton();
    }
    
    loginWithCustomUser(email, password) {
        browser.maximizeWindow();
        browser.url('https://165.227.137.250');

        page.enterEmail(email);
        page.enterPassword(password);
        page.clickLoginButton();
    }
    
    startEditingItemInList(name) {
        
        var theItemNumber;
        const allItemNames = $$('h3>a');
        for (var i = 0; i < allItemNames.length; i++) {
            if (allItemNames[i].getText() === name) {
                theItemNumber = i;
                break;
            }
        }
        
        const theEditButton = $$('a.button.is-info')[theItemNumber];
        theEditButton.click();
        
    }

//    selectItemFromList() {
//       
//    }

    

            

}

module.exports = HelpClass;