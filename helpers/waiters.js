class CustomWaits {
    forSpinner() {
        const spinner = $('div#preloader');
        spinner.waitForDisplayed(5000);
        spinner.waitForDisplayed(10000, true);

    }

    forNotificationToDisappear() {
        const notification = $('div.toast div');
        notification.waitForDisplayed(5000, true);
    }
    
    forListsToBeShown() {
        const editButton = $('a.button.is-info');
        editButton.waitForDisplayed(5000);
        browser.pause(100);
    }

}

module.exports = CustomWaits;