const ListsPage = require('../page/UserLists_po');
const page = new ListsPage();

class ListsActions {
    
    enterListName(value) {
        page.listNameInput.waitForDisplayed(2000);
        page.listNameInput.clearValue();
        page.listNameInput.setValue(value);
    }
    
    saveList() {
        page.saveListButton.waitForDisplayed(2000);
        page.saveListButton.click();
        browser.pause(2000);
    }
    
    navigateToCreateListPage() {
        page.addListButton.waitForDisplayed(2000);
        page.addListButton.click();
    }
    
    deleteList() {
        page.deleteListButton.waitForDisplayed(2000);
        page.deleteListButton.click();
        page.confirmDeleteListButton.waitForDisplayed(2000);
        page.confirmDeleteListButton.click();
    }
    
    getAmountOfUserLists() {
        browser.pause(2000);
        return page.amountOfUserLists;
    }

}

module.exports = ListsActions;
