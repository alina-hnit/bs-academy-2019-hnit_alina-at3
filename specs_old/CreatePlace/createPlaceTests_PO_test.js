const CreatePlaceActions = require('./actions/CreatePlace_pa')
const MenuActions = require('../Menu/pages/menu_pa');
const assert = require('assert');
const testData = require('./../testData.json');

const menuSteps = new MenuActions();
const pageSteps = new CreatePlaceActions();

function userLogin(email, password) {
    const emailField = $('input[name="email"]');
    const passField = $('input[type="password"]');
    const loginButton = $('button.is-primary');

    emailField.clearValue();
    emailField.setValue(email);
    passField.clearValue();
    passField.setValue(password);
    loginButton.click();
}

function waitForSpinner() {
    const spinner = $('div#preloader');
    spinner.waitForDisplayed(10000);
    spinner.waitForDisplayed(10000, true);
}

function waitForNotificationToDisappear() {
    const notification = $('div.toast div');
    notification.waitForDisplayed(5000, true);
}

function browserClick(el) {
    browser.execute(() => {
            document.querySelectorAll(el)[0].click();
        });
}

describe('Create new place tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(testData.appUrl);
        userLogin(testData.email, testData.password);
        waitForSpinner();
        menuSteps.navigateToNewPlace();
    });

    afterEach(() => {
        browser.reloadSession();
    });
    
    xit('create a place', () => {
        
        pageSteps.enterPlaceName(testData.placeName);
        pageSteps.enterPlaceLocation(testData.placeLocation);
        pageSteps.enterPlaceZip(testData.placeZip);
        pageSteps.enterPlaceAddress(testData.placeAddress);
        pageSteps.enterPlacePhone(testData.placePhone);
        pageSteps.enterPlaceWebsite(testData.placeWebsite);
        pageSteps.enterPlaceDescription(testData.placeDescription);
        
        pageSteps.goToNextPage();
        
        pageSteps.uploadPhotos('../../img/mb5.jpg');
        

    });


});